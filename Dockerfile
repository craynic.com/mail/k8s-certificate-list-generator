FROM alpine:3

ENV LABEL_SELECTOR=""

RUN apk --no-cache add --virtual build-dependencies curl~=8 \
    && curl -L -o /usr/bin/kubectl \
      https://storage.googleapis.com/kubernetes-release/release/v1.24.3/bin/linux/amd64/kubectl \
    && chmod +x /usr/bin/kubectl \
    && apk del build-dependencies \
    && apk add --no-cache \
      bash=~5 \
      openssl=~3 \
      diffutils=~3 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3

COPY files/ /

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]

CMD [""]
