# Relay Client Certificate Fingerprint updater (sidecar)

This container is intended as a sidecar for Postfix.

It searches for all Kubernetes `Certificate` with a given label and creates a fingerprint file with their SHA256
fingerprints.

The access to the cluster is through the `kubectl` binary (current version 1.24.0), the container needs to have all
the ENV variables set with a proper RBAC access, granting:

* `get` and `list` on all certificates across all namespaces
* `get` on secrets across all namespaces
 
The searched label on the certificates is:

```ini
craynic.net/mail-relay = true
```

The target file will be created at `/opt/fingerprints/fingerprints`. The parent folder `/opt/fingerprints/`
needs to be mounted as a volume.

The suggested way is to create an `emptyDir` both for this container and the consuming container:
```yaml
volumeMounts:
- mountPath: "/opt/fingerprints/"
  name: fingerprints
```

and declared as a volume for the pod:
```yaml
volumes:
- name: fingerprints
  emptyDir: {}
```
